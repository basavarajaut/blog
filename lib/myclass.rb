module MyModule
	class MyClass
		
		attr_accessor :foo
		
		attr_reader :bar
		
		attr_writer :bar
		
		def boo
			return @boo
		end
		def boo=(val)
			@boo = val
		end
		def min(x,y)
			if x < y then x else y end
		end	
		def self.cls_method
			return "MyClass type"
		end
	end	
end